# Flywheel Tutorials Repository

This repository hosts educational assets such as jupyter notebooks 
and supporting materials for webinars that demonstrate how one
can interact with a Flywheel instance using the flywheel-SDK. 

*  [Jump to Python tutorials](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/tree/master/python)
*  [Jump to Matlab tutorials](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/tree/master/matlab)
*  [Jump to webinars materials](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/tree/master/webinars)